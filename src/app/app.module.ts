import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavigationComponent } from './main/navigation/navigation.component';
import { HomeComponent } from './main/home/home.component';
import { ItemsListComponent } from './main/home/items-list/items-list.component';
import { ItemComponent } from './main/home/item/item.component';
import { ItemsService } from './Services/items.service';
import { MyCartComponent } from './main/my-cart/my-cart.component';
import { Routes, RouterModule } from "@angular/router";
import { RoutingService } from './Services/routing.service';
import { MyCartItemComponent } from './main/my-cart/my-cart-item/my-cart-item.component';
import { AddItemsComponent } from './main/add-items/add-items.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';

export const firebaseConfig = {
  apiKey: "AIzaSyBdJSarRd8A1miEPd7eiQu2c52slAZCCLc",
  authDomain: "observable-practice.firebaseapp.com",
  databaseURL: "https://observable-practice.firebaseio.com",
  projectId: "observable-practice",
  storageBucket: "observable-practice.appspot.com",
  messagingSenderId: "211606902267"
};
const APP_ROUTES: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'myCart',
        component: MyCartComponent
      },
      {
        path: 'addItems',
        component: AddItemsComponent
      },
      {
        path:'',
        pathMatch: 'full',
        redirectTo: 'home'
      }
    ]
  }
]; 

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavigationComponent,
    HomeComponent,
    ItemsListComponent,
    ItemComponent,
    MyCartComponent,
    MyCartItemComponent,
    AddItemsComponent
  ],
  imports: [
    BrowserModule, 
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    ItemsService,
    RoutingService,
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

