import { Component, OnInit } from '@angular/core';
import { ItemsService } from '../Services/items.service';
import { item } from '../Shared/item';
import { Router,  ActivatedRoute } from '@angular/router';
import { RoutingService } from '../Services/routing.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  route;
  constructor(private itemService:ItemsService,private router: Router, private activatedRoute:ActivatedRoute,private routingService :RoutingService) {
    this.router.navigate(['/home']);
    this.routingService.setUrl(this.activatedRoute);
  }

  ngOnInit() {
    this.routingService.routingObservable$.subscribe((res)=>{
    this.route = res;
    console.log(res);
    });
    this.itemService.itemsObservable$.subscribe((res) =>{
      console.log(res);
    }, (err)=> {
      console.log(err);
    })
  }

}
